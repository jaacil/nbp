package lato.nbp.exchange.controller;

import lato.nbp.exchange.model.dto.CurrentRateDto;
import lato.nbp.exchange.model.dto.TotalRateDto;
import lato.nbp.exchange.service.current.ExchangeCurrentRatesService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ExchangeRatesApiImpl implements ExchangeRatesApi {

    private final ExchangeCurrentRatesService exchangeCurrentRatesService;

    public ExchangeRatesApiImpl(ExchangeCurrentRatesService exchangeCurrentRatesService) {
        this.exchangeCurrentRatesService = exchangeCurrentRatesService;
    }

    @Override
    public ResponseEntity<CurrentRateDto> getCurrentRateForCurrencyCode(
            @RequestParam String code,
            @RequestParam String date) {
        CurrentRateDto currentRateForCurrencyCode = exchangeCurrentRatesService.getCurrentRateForCurrencyCode(code, date);
        return ResponseEntity.ok(currentRateForCurrencyCode);
    }

    @Override
    public ResponseEntity<TotalRateDto> getTotalRateForCurrencies(
            @RequestParam List<String> codes,
            @RequestParam String date) {
        TotalRateDto totalRateForCurrencies = exchangeCurrentRatesService.getTotalRateForCurrencies(codes, date);
        return ResponseEntity.ok(totalRateForCurrencies);
    }
}
