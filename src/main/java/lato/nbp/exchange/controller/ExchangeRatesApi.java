package lato.nbp.exchange.controller;

import lato.nbp.exchange.model.dto.CurrentRateDto;
import lato.nbp.exchange.model.dto.TotalRateDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@RequestMapping("/exchange")
public interface ExchangeRatesApi {

    @GetMapping("/currency")
    ResponseEntity<CurrentRateDto> getCurrentRateForCurrencyCode(String code, String date);

    @GetMapping("/currency/total")
    ResponseEntity<TotalRateDto> getTotalRateForCurrencies(List<String> codes, String date);

}
