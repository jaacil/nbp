package lato.nbp.exchange.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;

@Getter
@AllArgsConstructor
@JsonIgnoreProperties
public class RatesForTotal {

    private final String effectiveDate;
    private final BigDecimal mid;

}
