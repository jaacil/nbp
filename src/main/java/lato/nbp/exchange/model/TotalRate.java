package lato.nbp.exchange.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;

import java.util.List;

@JsonIgnoreProperties
@AllArgsConstructor
public class TotalRate {

    private String code;
    private List<RatesForTotal> rates;

    public List<RatesForTotal> getRates() {
        return rates;
    }
}

