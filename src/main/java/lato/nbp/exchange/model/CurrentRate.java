package lato.nbp.exchange.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;

import java.util.List;

@JsonIgnoreProperties
@AllArgsConstructor
public class CurrentRate {

    private String code;
    private List<RatesForCurrent> rates;

    public List<RatesForCurrent> getRates() {
        return rates;
    }
}

