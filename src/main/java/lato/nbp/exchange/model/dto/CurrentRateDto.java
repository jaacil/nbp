package lato.nbp.exchange.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;

@AllArgsConstructor
@Getter
public class CurrentRateDto {

    private BigDecimal value;
    private String date;

}
