package lato.nbp.exchange.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.Map;

@AllArgsConstructor
@Getter
public class TotalRateDto {

    private Map<String, BigDecimal> midForCurrencies;
    private String date;

}
