package lato.nbp.exchange.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;

@AllArgsConstructor
@JsonIgnoreProperties
@Getter
public class RatesForCurrent {

    private final String effectiveDate;
    private final BigDecimal ask;



}
