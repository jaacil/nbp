package lato.nbp.exchange.service.current;

import lato.nbp.exchange.model.dto.CurrentRateDto;
import lato.nbp.exchange.model.dto.TotalRateDto;
import lato.nbp.exchange.nbp.NbpApi;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExchangeCurrentRatesService {

    private final NbpApi nbpApi;

    public ExchangeCurrentRatesService(NbpApi nbpApi) {
        this.nbpApi = nbpApi;
    }

    public CurrentRateDto getCurrentRateForCurrencyCode(String code, String date) {
        return nbpApi.getCurrentRateForCurrencyCode(code, date);
    }

    public TotalRateDto getTotalRateForCurrencies(List<String> codes, String date) {
        return nbpApi.getTotalRateForCurrencies(codes, date);
    }

}
