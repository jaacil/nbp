package lato.nbp.exchange.nbp;

import lato.nbp.exchange.model.CurrentRate;
import lato.nbp.exchange.model.RatesForCurrent;
import lato.nbp.exchange.model.RatesForTotal;
import lato.nbp.exchange.model.TotalRate;
import lato.nbp.exchange.model.dto.CurrentRateDto;
import lato.nbp.exchange.model.dto.TotalRateDto;
import lato.nbp.exchange.repository.CachedRateRepository;
import lato.nbp.exchange.repository.RateType;
import lato.nbp.exchange.repository.entity.CachedRate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class NbpApiImpl implements NbpApi {

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    private final WebClient webClient;
    private final CachedRateRepository cache;

    public NbpApiImpl(WebClient webClient, CachedRateRepository cache) {
        this.webClient = webClient;
        this.cache = cache;
    }

    @Override
    public CurrentRateDto getCurrentRateForCurrencyCode(String code, String date) {

        Optional<CachedRate> cached = cache.findByDateAndRateType(date, RateType.CURRENT);

        if (cached.isPresent()) {
            CachedRate cachedRate = cached.get();
            return new CurrentRateDto(cachedRate.getValue(), cachedRate.getDate());
        }

        return callApiForCurrentRateForCurrencyCode(code, date);
    }


    @Override
    public TotalRateDto getTotalRateForCurrencies(List<String> codes, String date) {

//        Optional<CachedRate> cached = cache.findByCodeAndDateAndRateType(code, date, RateType.TOTAL);

        Map<String, BigDecimal> totalForCurrencies = getSpecifiedRateForCode(codes, date);

        return new TotalRateDto(totalForCurrencies, date);
    }

    private CurrentRateDto callApiForCurrentRateForCurrencyCode(String code, String date) {
        LocalDate parsedDate = LocalDate.parse(date, FORMATTER);

        String url = String.format("/rates/%s/%s/%s", "C", code, parsedDate);

        ResponseEntity<CurrentRate> responseEntity = webClient.get()
                .uri(url)
                .retrieve()
                .toEntity(CurrentRate.class)
                .block();


        BigDecimal value = responseEntity
                .getBody()
                .getRates()
                .stream()
                .map(RatesForCurrent::getAsk)
                .findFirst()
                .orElseThrow();

        cache.save(new CachedRate(value, date, RateType.CURRENT));

        return new CurrentRateDto(value, date);
    }

    private Map<String, BigDecimal> getSpecifiedRateForCode(List<String> codes, String date) {

        LocalDate parsedDate = LocalDate.parse(date, FORMATTER);
        Map<String, BigDecimal> totalForCurrencies = new HashMap<>();

        for (String code : codes) {

            String url = String.format("/rates/%s/%s/%s", "A", code, parsedDate);

            ResponseEntity<TotalRate> responseEntity = webClient.get()
                    .uri(url)
                    .retrieve()
                    .toEntity(TotalRate.class)
                    .block();

            BigDecimal value = responseEntity
                    .getBody()
                    .getRates()
                    .stream()
                    .map(RatesForTotal::getMid)
                    .findFirst()
                    .orElseThrow();

            totalForCurrencies.put(code, value);
        }

//        cache.save(new CachedRate(value, date, RateType.TOTAL));

        return totalForCurrencies;
    }
}
