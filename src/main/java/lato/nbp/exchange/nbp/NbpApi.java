package lato.nbp.exchange.nbp;

import lato.nbp.exchange.model.dto.CurrentRateDto;
import lato.nbp.exchange.model.dto.TotalRateDto;

import java.util.List;

public interface NbpApi {

    CurrentRateDto getCurrentRateForCurrencyCode(String code, String date);
    TotalRateDto getTotalRateForCurrencies(List<String> codes, String date);

}
