package lato.nbp.exchange.repository;

public enum RateType {
    CURRENT,
    TOTAL
}
