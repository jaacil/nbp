package lato.nbp.exchange.repository.entity;

import jakarta.persistence.*;
import lato.nbp.exchange.repository.RateType;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Entity
@Table
@NoArgsConstructor
public class CachedRate {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    private BigDecimal value;

    private String date;

    private String code;

//    @OneToMany
//    private List<CachedRatesForTotal> cachedRatesForTotal;

    @Enumerated(value = EnumType.STRING)
    private RateType rateType;

    public BigDecimal getValue() {
        return value;
    }

    public String getDate() {
        return date;
    }

    public CachedRate(BigDecimal value, String date, RateType rateType) {
        this.value = value;
        this.date = date;
        this.rateType = rateType;
    }

//    public CachedRate(BigDecimal value, String date, RateType rateType, List<CachedRatesForTotal> cachedRatesForTotal) {
//        this.value = value;
//        this.date = date;
//        this.rateType = rateType;
//        this.cachedRatesForTotal = cachedRatesForTotal;
//    }
}
