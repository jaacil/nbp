package lato.nbp.exchange.repository;

import lato.nbp.exchange.repository.entity.CachedRate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CachedRateRepository extends JpaRepository<CachedRate, Long> {

    Optional<CachedRate> findByDateAndRateType(String date, RateType rateType);
    List<CachedRate> findByCodeAndDateAndRateType(String code, String date, RateType rateType);

}
